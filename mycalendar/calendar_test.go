package mycalendar

import (
	"testing"
)

func TestSimpleEvent(t *testing.T) {
	events := MyCalendar{}
	event := MyEvent{
		Description: "test",
		Kind:        "Internal",
		Start:       "2020-10-06T16:30:00+02:00",
		End:         "2020-10-06T17:30:00+02:00",
		Response:    "declined",
	}
	events.AddEvent(event)
	if got := events.events; len(got) != 1 {
		t.Errorf("events item is not exaclyt 1 but %v", len(got))
	}
}
