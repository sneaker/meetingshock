package mycalendar

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"time"
)

type MeetingType struct {
	Name    string `json:"name"`
	Color   string `json:"color"`
	Subject string `json:"subject"`
}

type MeetingTypes struct {
	MeetingTypes []MeetingType `json:"cathegories"`
}

func NewMeetingType(name string, color string, subject string) MeetingType {
	return MeetingType{
		Name:    name,
		Color:   color,
		Subject: subject,
	}
}

func (m *MeetingTypes) GetFromColor(s string) MeetingType {
	for i := 0; i < len(m.MeetingTypes); i++ {
		if s == m.MeetingTypes[i].Color {
			return m.MeetingTypes[i]
		}
	}
	return NewMeetingType("", "", "")
}

func (m *MeetingTypes) GetFromName(s string) MeetingType {
	for i := 0; i < len(m.MeetingTypes); i++ {
		if s == m.MeetingTypes[i].Name {
			return m.MeetingTypes[i]
		}
	}
	return NewMeetingType("", "", "")
}

type MyEvent struct {
	Description string
	Kind        MeetingType
	Response    string
	Start       string
	End         string
}

func NewMyEvent(desc string, color string, resp string, start string, end string, allTypes MeetingTypes) MyEvent {
	kind := allTypes.GetFromColor(color)
	return MyEvent{
		Description: desc,
		Kind:        kind,
		Response:    resp,
		Start:       start,
		End:         end,
	}
}

type MyCalendar struct {
	events []MyEvent
	Config MeetingTypes
}

func (e *MyCalendar) AddEvent(event MyEvent) {
	e.events = append(e.events, event)
}

func (e *MyCalendar) SetMeetingTypes(types MeetingTypes) {
	e.Config = types
}

func (e *MyCalendar) PrintEvents(otype string) {
	if otype == "pretty" {
		for _, event := range e.events {
			fmt.Printf("Event: %v %v %v\n", event, event.GetDuration(), event.Kind)
		}
	} else if otype == "json" {
		json.NewEncoder(os.Stdout).Encode(e.events)
	} else {
		log.Fatalf("Cannot produce %v output", otype)
	}
}

func (e *MyCalendar) FilterEvents(f string) MyCalendar {
	kind := e.Config.GetFromName(f)
	var copy MyCalendar
	for _, event := range e.events {
		if kind.Name == "All" || kind == event.Kind {
			copy.events = append(copy.events, event)
		}
	}
	return copy
}

func (e MyCalendar) GetTotalMeetingTime() time.Duration {
	var total time.Duration
	for _, event := range e.events {
		total += event.GetDuration()
	}
	return total
}

func (e MyEvent) GetDuration() time.Duration {
	startt, err := time.Parse(time.RFC3339, e.Start)
	if err != nil {
		log.Fatalf("Could not parse date %v", err)
	}
	endt, err := time.Parse(time.RFC3339, e.End)
	if err != nil {
		log.Fatalf("Could not parse date %v", err)
	}
	diff := endt.Sub(startt)
	return diff
}
