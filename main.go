package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"time"

	"gitlab.com/sneaker/meetingshock/mycalendar"
	"gitlab.com/sneaker/meetingshock/utils"
	"golang.org/x/oauth2/google"
	"google.golang.org/api/calendar/v3"
)

type ArgsConfig struct {
	d *int
	e *string
	f *string
	r *string
	o *string
}

func readArgs() ArgsConfig {
	d := flag.Int("d", 7, "number of days to count")
	e := flag.String("e", "", "Your own email of attendees to filter; {test@test.com}")
	f := flag.String("f", "all", "Cathegory to filter; {all, Internal, Project}")
	r := flag.String("r", "all", "Only show events with the following answer: accepted, needsAction, declined")
	o := flag.String("o", "pretty", "Output: pretty, json, yaml")
	flag.Parse()
	return ArgsConfig{
		d: d,
		e: e,
		f: f,
		r: r,
		o: o,
	}
}

func getMeetingCathegories() mycalendar.MeetingTypes {
	jsonFile, err := os.Open("config.json")
	if err != nil {
		log.Fatalf("Cannot read config.json %v", err)
	}
	defer jsonFile.Close()
	byteValue, _ := ioutil.ReadAll(jsonFile)

	var cathegories mycalendar.MeetingTypes

	json.Unmarshal([]byte(byteValue), &cathegories)

	//for i := 0; i < len(cathegories.MeetingTypes); i++ {
	//	fmt.Println("Name: " + cathegories.MeetingTypes[i].Name)
	//}
	return cathegories
}

func initGCalendar() *calendar.Service {
	b, err := ioutil.ReadFile("credentials.json")
	if err != nil {
		log.Fatalf("Unable to read credentials file: %v", err)
	}
	config, err := google.ConfigFromJSON(b, calendar.CalendarReadonlyScope)
	if err != nil {
		log.Fatalf("Unable to parse credentails file: %v", err)
	}
	client := utils.GetClient(config)
	srv, err := calendar.New(client)
	if err != nil {
		log.Fatalf("Cannot contact calendar %v", err)
	}
	return srv
}

func getMinMaxTime(args ArgsConfig) (string, string) {
	var tmin, tmax string
	if *args.d > 0 {
		tmin = time.Now().Format(time.RFC3339)
		tmax = time.Now().AddDate(0, 0, *args.d).Format(time.RFC3339)
	} else {
		tmin = time.Now().AddDate(0, 0, *args.d).Format(time.RFC3339)
		tmax = time.Now().Format(time.RFC3339)
	}
	return tmin, tmax
}

func createCalendar(srv *calendar.Service, tmin string, tmax string, args ArgsConfig) *mycalendar.MyCalendar {
	var cathegories = getMeetingCathegories()
	events, err := srv.Events.List("primary").ShowDeleted(false).
		SingleEvents(true).TimeMin(tmin).TimeMax(tmax).OrderBy("startTime").Do()
	if err != nil {
		log.Fatalf("Unable to retrieve events: %v", err)
	}

	var m mycalendar.MyCalendar
	m.SetMeetingTypes(cathegories)
	if len(events.Items) == 0 {
		fmt.Println("No upcoming events found.")
	} else {
		for _, item := range events.Items {
			for _, att := range item.Attendees {
				if att.Email != *args.e {
					continue
				}
				if *args.r != "all" && att.ResponseStatus != *args.r {
					continue
				}
				var ev = mycalendar.NewMyEvent(
					item.Summary,
					item.ColorId,
					att.ResponseStatus,
					item.Start.DateTime,
					item.End.DateTime,
					cathegories,
				)
				m.AddEvent(ev)
			}
		}
	}
	return &m
}

func main() {
	var args ArgsConfig = readArgs()
	var tmin, tmax = getMinMaxTime(args)
	var srv = initGCalendar()
	var m = createCalendar(srv, tmin, tmax, args).FilterEvents(*args.f)
	m.PrintEvents(*args.o)
	fmt.Printf("total Meeting time %v in %v", m.GetTotalMeetingTime(), *args.f)
}
