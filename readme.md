# Meetingshock

Small go binary to get an overview of meetings and appointments in your callendar.

## First run

In order to run you first need to authenticate to your account. In order to connect follow the Step1: in the [Google Calendar API HOWTO](https://developers.google.com/calendar/quickstart/go)

meetingshock will initiate this automatically on the first run. It save the credentials to a file called `credentials.json`

## How to compile:

```bash
go build
```

## How to run

To see the help text:

```bash
./meetingshock -h 
```

To get all meetings for the next 2 days in the cathegory Internal :

```bash
./meetingshock -e myself@brainrun.com -d 2 -f Internal -r accepted
```

## How to filter

The current concept on how to filter events is based on coloring of the events. You can find the coloring in the MeetingType Enum in events.go

## TODO

Further Ideas:

- make Cathegory configuration not to be hardcoded but in a configfile
- make pretty printing (some code there)
- select a timerange and not always go from now
